package be.kdg.neo4j.repository;

import be.kdg.neo4j.model.PhoneNumber;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "PhoneNumbers", path = "PhoneNumbers")
@Repository
public interface PhoneNumberRepository extends Neo4jRepository<PhoneNumber,Long> {
    Optional<PhoneNumber> findByPhoneNumber(String phoneNumber);

}
