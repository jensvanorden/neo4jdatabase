package be.kdg.neo4j.repository;

import be.kdg.neo4j.model.Loan;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource(collectionResourceRel = "Loans", path = "Loans")
@Repository
public interface LoanRepository extends Neo4jRepository<Loan,Long> {
}
