package be.kdg.neo4j.repository;

import be.kdg.neo4j.model.SSN;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "SSNs", path = "SSNs")
@Repository
public interface SSNRepository  extends Neo4jRepository<SSN,Long> {
    Optional<SSN>findBySsnNumber(String ssn);
}
