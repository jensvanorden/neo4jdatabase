package be.kdg.neo4j.repository;

import be.kdg.neo4j.model.BankAccount;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource(collectionResourceRel = "BankAccounts", path = "BankAccounts")
@Repository
public interface BankAccountRepository extends Neo4jRepository<BankAccount,Long> {
}
