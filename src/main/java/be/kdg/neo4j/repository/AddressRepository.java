package be.kdg.neo4j.repository;


import be.kdg.neo4j.model.Address;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "Addresses", path = "Addresses")
@Repository
public interface AddressRepository extends Neo4jRepository<Address,Long> {

   /* @Query("MATCH(a:AccountHolder) return a")
    Collection<AccountHolder> getAllAccountHolders();*/
   Optional<Address> findByStreetAndCityAndZipCode(String street, String city, Integer zipcode);

}