package be.kdg.neo4j.repository;

import be.kdg.neo4j.model.AccountHolder;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource(collectionResourceRel = "AccountHolders", path = "AccountHolders")
@Repository
public interface AccountHolderRepository extends Neo4jRepository<AccountHolder,Long> {

   /* @Query("MATCH(a:AccountHolder) return a")
    Collection<AccountHolder> getAllAccountHolders();*/


}

