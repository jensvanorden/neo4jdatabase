package be.kdg.neo4j.repository;

import be.kdg.neo4j.model.CreditCard;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource(collectionResourceRel = "CreditCards", path = "CreditCards")
@Repository
public interface CreditCardRepository extends Neo4jRepository<CreditCard,Long> {
}
