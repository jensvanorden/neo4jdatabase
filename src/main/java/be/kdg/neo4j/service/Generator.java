package be.kdg.neo4j.service;

import java.time.LocalDate;
import java.util.Random;

class Generator {
    private Random random = new Random(System.currentTimeMillis());

    String getCardNumber(String bin) {
        int randomNumberLength = 16 - (bin.length() + 1);

        StringBuilder builder = new StringBuilder(bin);
        for (int i = 0; i < randomNumberLength; i++) {
            int digit = this.random.nextInt(10);
            builder.append(digit);
        }

        return builder.toString();
    }

    String getExpirationDate() {
        return LocalDate.now().plusYears(6).toString();
    }

    Integer getSecurityCode() {
        return random.nextInt(900)+100;
    }

    Double getBalance() {
        return ((double) random.nextInt(2000000))/100;
    }

    Integer getCreditLimit() {
        return 100000;
    }

    Double getLoanAmount() {
        return (double) random.nextInt(1000)*1000;
    }
}
