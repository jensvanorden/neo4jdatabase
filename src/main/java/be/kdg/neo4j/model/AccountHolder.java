package be.kdg.neo4j.model;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@Getter
@Setter
@NodeEntity
public class AccountHolder {
    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;

    @Relationship(type = "HAS_ADDRESS")
    private Address address;
    @Relationship(type = "HAS_PHONENUMBER")
    private PhoneNumber phoneNumber;
    @Relationship(type = "HAS_SSN")
    private SSN ssn;

    @Relationship(type = "HAS_BANKACCOUNT")
    private List<BankAccount> bankAccounts;


    @Relationship(type = "HAS_CREDITCARD")
    private List<CreditCard> creditCards;

    @Relationship(type = "HAS_LOAN")
    private List<Loan> loans;


}
