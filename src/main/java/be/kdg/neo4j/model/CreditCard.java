package be.kdg.neo4j.model;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@Getter
@Setter
@NodeEntity
public class CreditCard {
    @Id
    @GeneratedValue
    private Long id;
    private String cardNumber;
    private Integer creditLimit;
    private Double balance;
    private String expirationDate;
    private Integer securityCode;


}
