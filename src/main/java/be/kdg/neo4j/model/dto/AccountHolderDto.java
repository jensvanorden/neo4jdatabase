package be.kdg.neo4j.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountHolderDto {
    private String firstName;
    private String lastName;
    private String ssnNumber;
    private String street;
    private String city;
    private Integer zipCode;
    private String phoneNumber;
    private boolean bankAccount;
    private boolean creditCard;
    private boolean loan;

    @Override
    public String toString() {
        return "AccountHolderDto{" +
                "\"firstName\" : \"" + firstName + '\"' +
                ", \"lastName\" : \"" + lastName + '\"' +
                ", \"ssnNumber\" : \"" + ssnNumber + '\"' +
                ", \"street\" : \"" + street + '\"' +
                ", \"city\" : \"" + city + '\"' +
                ", \"zipCode\" : " + zipCode +
                ", \"phoneNumber\" : \"" + phoneNumber + '\"' +
                ", \"bankAccount\" : " + bankAccount +
                ", \"creditCard\" : " + creditCard +
                ", \"loan\" : " + loan +
                '}';
    }
}
