package be.kdg.neo4j.controller;

import be.kdg.neo4j.model.AccountHolder;
import be.kdg.neo4j.model.dto.AccountHolderDto;
import be.kdg.neo4j.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {
    private AccountService accountService;
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/post")
    public ResponseEntity<AccountHolder> postAccountHolder(@RequestBody String dtoString) {
        System.out.println(dtoString);
        try {
            AccountHolderDto dto = mapper.readValue(dtoString, AccountHolderDto.class);
            AccountHolder holder = accountService.createAccountHolder(dto);
            return ResponseEntity.ok(holder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(null);
    }

    
}
